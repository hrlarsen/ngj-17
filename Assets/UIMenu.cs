﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMenu : MonoBehaviour {

    public GameObject main;
    public GameObject credits;
    public GameObject end;

    void Start()
	{
        main.SetActive(true);
        credits.SetActive(false);
    }

    public void StartGame()
	{
        GameManager.Instance.StartGame();
        gameObject.SetActive(false);
    }

	public void ShowCredits()
	{
        main.SetActive(false);
        credits.SetActive(true);
    }

	public void HideCredits()
	{
        main.SetActive(true);
        credits.SetActive(false);
    }
}
