﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoTextManager : MonoBehaviour {
[	SerializeField]
    GameObject[] activateObjectsOnFirstDeath;
    [SerializeField]
    GameObject[] activateObjectsOnSecondDeath;
    [SerializeField]
    GameObject[] activateObjectsOnThirdDeath;

    public static InfoTextManager Instance;
	void Awake()
	{
		if(Instance == null)
            Instance = this;
    }


    public void PlayerDied(int deathCount)
	{ 
		if (deathCount == 0)
        {
            foreach (GameObject go in activateObjectsOnFirstDeath)
            {
                go.SetActive(true);
            }
            if (deathCount == 1)
                foreach (GameObject go in activateObjectsOnSecondDeath)
                {
                    go.SetActive(true);
                }
            if (deathCount == 2)
                foreach (GameObject go in activateObjectsOnThirdDeath)
                {
                    go.SetActive(true);
                }
        }

	}
}
