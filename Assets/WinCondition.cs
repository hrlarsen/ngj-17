﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinCondition : MonoBehaviour {

	void OnTriggerEnter(Collider other)
	{
		if(other.CompareTag("Player")){
            GameManager.Instance.WinLevel();
        }
	}
}
