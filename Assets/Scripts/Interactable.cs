﻿using UnityEngine;

public abstract class Interactable : MonoBehaviour {
    public bool isInteractable;
    public bool isInteracting;

    public virtual void Interact()
	{
        isInteracting = true;
    }

	public virtual void StopInteracting()
	{
        isInteracting = false;
    }

	public virtual void OnEnterInteractionArea(GameObject intruder)
	{
		if(!isInteractable)
            return;
		if(intruder.GetComponent<InteractableController>() != null)
        	intruder.GetComponent<InteractableController>().InteractableWithinReach(this);
    }

	public virtual void OnExitInteractionArea(GameObject extruder)
	{
		if(extruder.GetComponent<InteractableController>() != null)
        	extruder.GetComponent<InteractableController>().InteractableOutOfReach(this);
    }
}
