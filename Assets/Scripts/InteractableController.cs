﻿using System.Collections.Generic;
using UnityEngine;

public class InteractableController : MonoBehaviour {
    private List<Interactable> currentInteractables = new List<Interactable>();

    Player player;
    private void Start()
    {
        player = GameObject.FindObjectOfType<Player>();
    }

    public void InteractableWithinReach(Interactable interactable)
	{
		if(!currentInteractables.Contains(interactable))
        	currentInteractables.Add(interactable);
    }

	public void InteractableOutOfReach(Interactable interactable)
	{
		if(currentInteractables.Contains(interactable))
        	currentInteractables.Remove(interactable);
    }

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.Space))
		{
            if (currentInteractables.Count > 0)
            {
                for (int i = 0; i < currentInteractables.Count; i++)
                {
                    currentInteractables[i].Interact();
                }
                player.anim.SetBool("isInteracting", true);
            }

        } else if(Input.GetKeyUp(KeyCode.Space))
		{
			if (currentInteractables.Count > 0)
            {
                for (int i = 0; i < currentInteractables.Count; i++)
                {
                    currentInteractables[i].StopInteracting();
                }
                player.anim.SetBool("isInteracting", false);
            }
        }
	}
}
