﻿using UnityEngine;

public class Pushable : Interactable {
    private AudioSource audioSource;
    private Rigidbody rb;
    void Start()
	{
		gameObject.layer = LayerMask.NameToLayer("IgnorePlayer");
        audioSource = GetComponent<AudioSource>();
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
		
		if (rb.velocity.y > 0)
		if (rb.velocity.y > 4f) {
			rb.velocity = rb.velocity.normalized * 2;
		}
		if (rb.velocity.y > 0)
		if (rb.velocity.x > 4f || rb.velocity.x < -4f) {
			rb.velocity = rb.velocity.normalized * 2;
		}
        if(Mathf.Abs(rb.velocity.x) > 0.01f)
        {
            if (isInteractable && !audioSource.isPlaying)
            {
                audioSource.Play();
                audioSource.loop = true;
            }
        } else
        {
            audioSource.Stop();
        }
    }

    public override void Interact()
	{
        base.Interact();
        gameObject.layer = LayerMask.NameToLayer("Default");
    }

	public override void StopInteracting()
	{
        base.StopInteracting();
		gameObject.layer = LayerMask.NameToLayer("IgnorePlayer");
    }
}
