﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    public static GameManager Instance;
    public UIMenu menu;
    private int currentLevel = 0;
    public GameObject playerPrefab;
    private GameObject player;


    void Awake()
	{
		if(Instance == null)
            Instance = this;
    }

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }

	public void WinLevel()
	{
        Debug.Log("FUCK YAS YOU WON!");
		if(player == null)
            player = GameObject.FindObjectOfType<Player>().gameObject;
        Destroy(player);
        menu.gameObject.SetActive(true);
    }

	public void StartGame()
	{
        player =  GameObject.Instantiate(playerPrefab, Vector3.up, Quaternion.Euler(Vector3.right));
        Camera.main.GetComponent<SmoothFollow>().target = player.transform;
    }

}
