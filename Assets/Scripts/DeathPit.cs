﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathPit : MonoBehaviour {
    [SerializeField]
    GameObject deathPitSpikeFab;

    int spikes;

    private void Start()
    {
        spikes = Random.Range(4, 10);
        for (int i = 0; i < spikes; i++)
        {
            Vector3 pos = new Vector3(Random.Range(-1.5f, 1.5f) + transform.position.x, transform.position.y + 2.7f, Random.Range(-1f, 2f));
            Quaternion rot = Quaternion.Euler(new Vector3(Random.Range(0f, 20f), Random.Range(0f, 90f), Random.Range(0f, 20f)));
            GameObject go = Instantiate<GameObject>(deathPitSpikeFab, pos, rot, transform);
            
            //go.transform.SetParent(transform);
        }
    }
    void OnTriggerEnter(Collider other)
	{
		if(other.GetComponent<Entity>())
		{
            other.GetComponent<Entity>().Die();
            // DIE!
        }
	}
}
