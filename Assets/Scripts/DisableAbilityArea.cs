﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableAbilityArea : MonoBehaviour {
	public enum Ability{
		Jump,
		Push
	}
    public Ability abilityToDisable;

	void OnTriggerEnter(Collider other)
	{
		if(other.CompareTag("Player"))
		{
            ActivateAbility(other.gameObject, false);
        }
	}

	void OnTriggerExit(Collider other)
	{
        if (other.CompareTag("Player"))
        {
			ActivateAbility(other.gameObject, true);
        }
    }

	void ActivateAbility(GameObject player, bool activate)
    {
        switch (abilityToDisable)
        {
            case Ability.Jump:
                player.GetComponent<Jump>().enabled = activate;
                break;
            case Ability.Push:
				//player.GetComponent<Pus>().enabled = activate;
                break;
        }
    }
}
