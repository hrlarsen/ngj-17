﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionArea : MonoBehaviour {

    Interactable interactable;
    // Use this for initialization
    void Start () {
        interactable = GetComponentInParent<Interactable>();
        gameObject.layer = LayerMask.NameToLayer("Interactable");
    }
	
	void OnTriggerEnter(Collider other)
	{
        if(interactable.isInteractable)
            interactable.OnEnterInteractionArea(other.gameObject);
    }

	void OnTriggerExit(Collider other)
	{
        interactable.OnExitInteractionArea(other.gameObject);
    }
}
