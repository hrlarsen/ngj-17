﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {
    Rigidbody rb;
    [SerializeField]
    public float movementAcceleration;
    [SerializeField]
    public float movementAccelerationAirborne;
    [SerializeField]
    public float walkSpeedCap;
    public Player player;
    // Use this for initialization
    public virtual void Start () {
        rb = GetComponent<Rigidbody>();
        player = GetComponent<Player>();
    }
	
	// Update is called once per frame
	public virtual void FixedUpdate () {
        
        if (player.isDead) return;
        Vector3 movementDirection = rb.position;

        float getAxis = Input.GetAxis("Horizontal");
		if (rb.velocity.x > walkSpeedCap || rb.velocity.x < -walkSpeedCap) {
			rb.velocity = rb.velocity.normalized * walkSpeedCap;
		}
        movementDirection.x += getAxis* 3f * Time.fixedDeltaTime;
        
        if(getAxis > 0) player.mesh.transform.eulerAngles = new Vector3(0, 90f, 0);
        if (getAxis < 0) player.mesh.transform.eulerAngles = new Vector3(0, -90f, 0);

        if (getAxis == 0f) player.anim.SetBool("isWalking", false);
        else
        {
            player.anim.SetBool("isWalking", true);
           //if (player.IsGrounded()) player.anim.SetBool("isAirborne", false);
        }
        rb.MovePosition(movementDirection);
    }

    /*public void MoveDirection(float direction)
    {
        Vector3 newPosition = transform.position + new Vector3(direction * Time.fixedDeltaTime, 0f);
        rb.MovePosition(newPosition);
       // rb.AddForce(new Vector3(direction,0f));
       // rb.velocity = new Vector3(Mathf.Max(rb.velocity.x,-walkSpeedCap),rb.velocity.y);
       // rb.velocity = new Vector3(Mathf.Min(rb.velocity.x, walkSpeedCap), rb.velocity.y);
    }*/
}
