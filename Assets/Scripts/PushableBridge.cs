﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushableBridge : Entity {
    public Collider bridgeCollider;
    [SerializeField]
    AudioClip audioClip;
	void Start()
	{
        bridgeCollider.enabled = false;
    }

    public override void Die()
    {
        AudioSource audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.clip = audioClip;
        audioSource.loop = false;
        audioSource.Play();
        
        bridgeCollider.enabled = true;
        GetComponent<Pushable>().isInteractable = false;

    }
}
