﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour {
    Rigidbody rb;
    [SerializeField]
    float jumpHeight;
    Player player;
    AudioSource audioSource;
    [SerializeField]
    AudioClip[] jumpClips;
    bool finished;
    [SerializeField]
    GameObject text;
    [SerializeField]
    float distanceBeforeAppearing;
	// Use this for initialization
	void Start ()
    {
        rb = GetComponent<Rigidbody>();
        player = GetComponent<Player>();
        audioSource = GetComponent<AudioSource>();

	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (finished) return;
        if (transform.position.y < distanceBeforeAppearing) finished = true;
        if (player.isDead) return;
		if(Input.GetKeyDown(KeyCode.W) && player.IsGrounded())
        {
            rb.drag = 0.000001f;
            rb.velocity = new Vector3(rb.velocity.x, jumpHeight);
            audioSource.clip = jumpClips[(int)Random.Range(0,jumpClips.Length - 0.000001f)];
            audioSource.Play();
        }
	}

    private void Update()
    {
        if (finished)//Game ends
        {
            text.SetActive(true);
        }
    }


}
