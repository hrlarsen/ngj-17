﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaiseWater : MonoBehaviour {
    [SerializeField]
    float amountToRaiseBy;
    [SerializeField]
    float raiseTime;
    float timer;
    

    public bool raiseTheWater;
    bool done;

    Vector3 initialPosition;


    void Update()
    {
        if (done) return;
        if (raiseTheWater)
        {
            if (timer + raiseTime > Time.time)
            {
                transform.position = Vector3.Lerp(initialPosition, initialPosition + new Vector3(0f, amountToRaiseBy), (Time.time - timer) / raiseTime);
            }
            else transform.position = initialPosition + new Vector3(0f, amountToRaiseBy);
        }
    }

    private void Start()
    {
        initialPosition = transform.position;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Boulder"))
        {
            Physics.IgnoreCollision(other, GetComponent<Collider>());
            GetComponent<BoxCollider>().isTrigger = false;
            raiseTheWater = true;
            timer = Time.time;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.CompareTag("Player"))//FIRE SWIM ANIMATION
        {
            collision.collider.GetComponent<Player>().anim.SetBool("isSwimming", true);
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.CompareTag("Player"))//FIRE SWIM ANIMATION
        {
            collision.collider.GetComponent<Player>().anim.SetBool("isSwimming", false);
        }
    }
}
