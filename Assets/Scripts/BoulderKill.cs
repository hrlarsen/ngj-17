﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoulderKill : MonoBehaviour {
    Rigidbody rb;
    AudioSource audioSource;
    bool onPlayerLayer;
    [SerializeField]
    AudioClip[] clips;
    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {


        if (!onPlayerLayer)
        if (rb.velocity.x > 1.5f || rb.velocity.x < -1.5f)
        {
                gameObject.layer = LayerMask.NameToLayer("Default");
                onPlayerLayer = true;
                audioSource.Play();
        }

        if (onPlayerLayer)
            if(rb.velocity.x < 1.5f && rb.velocity.x > -1.5f)
        {
                onPlayerLayer = false;
                audioSource.Pause();
                gameObject.layer = LayerMask.NameToLayer("IgnorePlayer");
            }
	}

    private void OnCollisionEnter(Collision collision)
    {
        if(!enabled)
            return;
        if (collision.collider.CompareTag("Player")){
            if (Input.GetKey(KeyCode.E)) return;
            if ((rb.velocity.x < 0.05f) && (rb.velocity.x > -0.05f)) return;
            Transform player = collision.collider.transform;
            player.GetComponent<Player>().Die();
            audioSource.clip = clips[1];
            audioSource.Play();
            audioSource.loop = false;
            StartCoroutine("StopSounds");
        }
    }

    IEnumerator StopSounds()
    {
        yield return new WaitForSeconds(2f);
        audioSource.Stop();
        gameObject.layer = LayerMask.NameToLayer("Default");
        yield return 0;
    }
}
