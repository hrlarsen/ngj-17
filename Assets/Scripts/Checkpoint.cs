﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour {
    Player player;
    [SerializeField]
    GameObject particleSystem;

    public AudioClip activateObjectsSound;
    public GameObject[] objectsToActivate;
    private bool isChecked = false;

    void Start()
    {
        ActivateObjects(false);
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (player == null) player = collision.GetComponent<Player>();
            int checkPoint;
            if(!int.TryParse(transform.name,out checkPoint))checkPoint = 0;
            if (int.Parse(player.checkPoint) < checkPoint)
            {
                player.checkPoint = checkPoint.ToString();
                particleSystem.SetActive(true);
            }
        }
        ActivateObjects(true);
        isChecked = true;
    }

    private void ActivateObjects(bool active)
    {
        if(!isChecked && objectsToActivate.Length > 0)
        {
            for (int i = 0; i < objectsToActivate.Length; i++){
                objectsToActivate[i].SetActive(active);
            }

            if(active)
            {
                GameObject go = GameObject.Instantiate(new GameObject(), Camera.main.transform.position, Quaternion.identity);
                Light light = go.AddComponent<Light>();
                light.type = LightType.Point;
                light.range = 50;
                light.intensity = 3;

                Destroy(go, 0.3f);
                AudioSource source = gameObject.AddComponent<AudioSource>();
                source.clip = activateObjectsSound;
                source.loop = false;
                source.Play();
            }
        }   
    }
}
