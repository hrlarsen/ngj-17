﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Entity {
    [HideInInspector]
    public string checkPoint = "-1";
    [SerializeField]
    GameObject blood;

    Vector3 bloodStartPos;
    Quaternion bloodStartRot;
    Quaternion startRot;
    Rigidbody rb;
    public GameObject mesh;
    
    public Animator anim;
    public int deathCount = 1;

    public bool isDead;

    private void Start()
    {
        anim = GetComponentInChildren<Animator>();
        bloodStartPos = blood.transform.localPosition;
        bloodStartRot = blood.transform.rotation;
        startRot = transform.rotation;
        rb = GetComponent<Rigidbody>();
    }

    public override void Die()
    {
        anim.SetBool("isWalking", false);
        StartCoroutine("Death");
        blood.SetActive(true);
        blood.transform.SetParent(null);
        isDead = true;
        rb.freezeRotation = false;
        rb.AddTorque(new Vector3(2f, 2f, 2f));
    }

    IEnumerator Death()
    {
        yield return new WaitForSeconds(3f);
        blood.transform.SetParent(transform);
        blood.transform.localPosition = bloodStartPos;
        blood.transform.rotation = bloodStartRot;
        blood.SetActive(false);
        transform.rotation = startRot;
        rb.freezeRotation = true;
        RespawnAtCheckpoint();
        isDead = false;
        deathCount++;
        yield return null;
    }

    public bool IsGrounded()
    {
        Ray ray = new Ray(transform.position, -transform.up);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, transform.localScale.y))
        {
            anim.SetBool("isAirborne", false);
            return true;
            
        }
        else
        {
            anim.SetBool("isAirborne", true);
            return false;
            
        }
    }

    public void RespawnAtCheckpoint()
    {
        InfoTextManager.Instance.PlayerDied(deathCount);
        foreach (Transform t in GameObject.Find("Checkpoints").GetComponentInChildren<Transform>())
        {
            if (t.name == checkPoint)
            {
                transform.position = t.position;
                break;
            }
        }
    }
}
