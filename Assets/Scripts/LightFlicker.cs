﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFlicker : MonoBehaviour {
    Light light;
    [SerializeField]
    Color from;
    [SerializeField]
    Color to;

    Color col;
    float seconds;
    float timer;
	// Use this for initialization
	void Start () {
        light = GetComponent<Light>();
        from = to - from;
        seconds = 1f;
        timer = Time.time + seconds;
        col = to - from * Random.value;
    }
	
	// Update is called once per frame
	void Update () {
        if(Time.time > timer)
        {
            timer = Time.time + Random.value * 0.2f;
            col = from * Random.value;
            col = to - col;
        }

        light.color = Color.Lerp(light.color,col,(timer - Time.time) / seconds * Time.deltaTime);
        light.intensity = 0.7f + (timer - Time.time) / seconds;
    }
}
