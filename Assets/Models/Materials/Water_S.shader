// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Water_S"
{
	Properties
	{
		[HideInInspector] __dirty( "", Int ) = 1
		_TextureSample3("Texture Sample 3", 2D) = "bump" {}
		_Beach_NRM("Beach_NRM", 2D) = "bump" {}
		_TextureSample1("Texture Sample 1", 2D) = "white" {}
		_Beach_M("Beach_M", 2D) = "white" {}
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" }
		Cull Back
		CGINCLUDE
		#include "UnityShaderVariables.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) fixed3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float3 worldPos;
			float3 worldNormal;
			INTERNAL_DATA
		};

		uniform sampler2D _Beach_NRM;
		uniform sampler2D _TextureSample3;
		uniform sampler2D _Beach_M;
		uniform sampler2D _TextureSample1;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float4 _Color1 = float4(0,0,1,0);
			float3 ase_worldPos = i.worldPos;
			float2 componentMask48 = ase_worldPos.xz;
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			o.Normal = lerp( _Color1 , lerp( _Color1 , float4( ( UnpackNormal( tex2D( _Beach_NRM,(abs( ( ( componentMask48 * clamp( ase_worldNormal.y , 0.0 , 1.0 ) ) / 10.0 )+_Time[1] * float2(0.02,0.1 )))) ) * UnpackNormal( tex2D( _TextureSample3,(abs( ( ( componentMask48 * clamp( ase_worldNormal.y , 0.0 , 1.0 ) ) / 10.0 )+_Time[1] * float2(-0.02,0.09 )))) ) ) , 0.0 ) , 0.5 ) , ase_worldNormal.y );
			float4 _Color0 = float4(0,0.1724138,1,0);
			o.Albedo = lerp( _Color0 , ( _Color0 * ( tex2D( _Beach_M,(abs( ( ( componentMask48 * clamp( ase_worldNormal.y , 0.0 , 1.0 ) ) / 10.0 )+_Time[1] * float2(0.02,0.1 )))).r * tex2D( _TextureSample1,(abs( ( ( componentMask48 * clamp( ase_worldNormal.y , 0.0 , 1.0 ) ) / 10.0 )+_Time[1] * float2(-0.02,0.09 )))).r ) ) , ase_worldNormal.y ).rgb;
			o.Smoothness = 0.85;
			o.Alpha = lerp( 0.86 , 0.99 , ase_worldPos.y );
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Standard alpha:fade keepalpha 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			# include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float3 worldPos : TEXCOORD6;
				float4 tSpace0 : TEXCOORD1;
				float4 tSpace1 : TEXCOORD2;
				float4 tSpace2 : TEXCOORD3;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				fixed3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				fixed tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				fixed3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			fixed4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				fixed3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=7003
570;97;1352;692;3064.992;193.1005;1.6;True;False
Node;AmplifyShaderEditor.WorldNormalVector;56;-1995.501,406.2001;Float;False;0;FLOAT3;0,0,0;False;FLOAT3;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.WorldPosInputsNode;43;-1919.717,-59.9332;Float;False;FLOAT3;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;53;-1748.403,123.9998;Float;False;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;FLOAT
Node;AmplifyShaderEditor.ComponentMaskNode;48;-1574.307,-76.00016;Float;False;True;False;True;False;0;FLOAT3;0,0,0,0;False;FLOAT2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;52;-1532.402,21.59999;Float;False;0;FLOAT2;0.0;False;1;FLOAT;0.0,0;False;FLOAT2
Node;AmplifyShaderEditor.RangedFloatNode;50;-1442.805,147.1998;Float;False;Constant;_Float3;Float 3;4;0;10;0;0;FLOAT
Node;AmplifyShaderEditor.SimpleDivideOpNode;49;-1345.203,1.599761;Float;False;0;FLOAT2;0.0;False;1;FLOAT;0,0;False;FLOAT2
Node;AmplifyShaderEditor.PannerNode;14;-1149.3,550.0002;Float;False;-0.02;0.09;0;FLOAT2;0,0;False;1;FLOAT;0.0;False;FLOAT2
Node;AmplifyShaderEditor.PannerNode;11;-1143.8,296.0001;Float;False;0.02;0.1;0;FLOAT2;0,0;False;1;FLOAT;0.0;False;FLOAT2
Node;AmplifyShaderEditor.SamplerNode;2;-932.7997,269.0001;Float;True;Property;_Beach_M;Beach_M;1;0;Assets/Models/Materials/Beach_M.png;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;13;-928.2997,459.0002;Float;True;Property;_TextureSample1;Texture Sample 1;1;0;Assets/Models/Materials/Beach_M.png;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;1;-928.7997,650.9996;Float;True;Property;_Beach_NRM;Beach_NRM;0;0;Assets/Models/Materials/Beach_NRM.png;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;FLOAT3;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;16;-933.9996,839.1998;Float;True;Property;_TextureSample3;Texture Sample 3;0;0;Assets/Models/Materials/Beach_NRM.png;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;FLOAT3;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ColorNode;9;-782,-325;Float;False;Constant;_Color0;Color 0;2;0;0,0.1724138,1,0;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;15;-564.7994,297.0002;Float;False;0;FLOAT;0.0;False;1;FLOAT;0.0;False;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;39;-511.9027,846.7819;Float;False;Constant;_Float0;Float 0;4;0;0.5;0;0;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;17;-521.2001,725.3994;Float;False;0;FLOAT3;0.0;False;1;FLOAT3;0.0,0,0;False;FLOAT3
Node;AmplifyShaderEditor.ColorNode;38;-522.103,507.6823;Float;False;Constant;_Color1;Color 1;4;0;0,0,1,0;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;61;-2295.394,389.2995;Float;False;Constant;_Float1;Float 1;4;0;0.86;0;0;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;62;-2340.192,494.8996;Float;False;Constant;_Float4;Float 4;4;0;0.99;0;0;FLOAT
Node;AmplifyShaderEditor.LerpOp;37;-97.20303,618.4819;Float;False;0;COLOR;0.0,0,0;False;1;FLOAT3;0.0,0,0,0;False;2;FLOAT;0.0;False;COLOR
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;19;-397,-241;Float;False;0;COLOR;0.0;False;1;FLOAT;0.0,0,0,0;False;COLOR
Node;AmplifyShaderEditor.LerpOp;54;-177.7998,-118.7999;Float;False;0;COLOR;0,0,0,0;False;1;COLOR;0.0;False;2;FLOAT;0.0;False;COLOR
Node;AmplifyShaderEditor.LerpOp;55;24.29988,396.5997;Float;False;0;COLOR;0.0,0,0,0;False;1;COLOR;0.0,0,0,0;False;2;FLOAT;0,0,0,0;False;COLOR
Node;AmplifyShaderEditor.LerpOp;60;-2141.794,544.0999;Float;False;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;25;-208.4001,48.80009;Float;False;Constant;_Float2;Float 2;5;0;0.85;0;0;FLOAT
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;703.9999,-81.89998;Float;False;True;2;Float;ASEMaterialInspector;0;Standard;Water_S;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;Transparent;0.5;True;True;0;False;Transparent;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;False;0;4;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;Add;Add;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;Relative;0;;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;OBJECT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;13;OBJECT;0.0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False
WireConnection;53;0;56;2
WireConnection;48;0;43;0
WireConnection;52;0;48;0
WireConnection;52;1;53;0
WireConnection;49;0;52;0
WireConnection;49;1;50;0
WireConnection;14;0;49;0
WireConnection;11;0;49;0
WireConnection;2;1;11;0
WireConnection;13;1;14;0
WireConnection;1;1;11;0
WireConnection;16;1;14;0
WireConnection;15;0;2;1
WireConnection;15;1;13;1
WireConnection;17;0;1;0
WireConnection;17;1;16;0
WireConnection;37;0;38;0
WireConnection;37;1;17;0
WireConnection;37;2;39;0
WireConnection;19;0;9;0
WireConnection;19;1;15;0
WireConnection;54;0;9;0
WireConnection;54;1;19;0
WireConnection;54;2;56;2
WireConnection;55;0;38;0
WireConnection;55;1;37;0
WireConnection;55;2;56;2
WireConnection;60;0;61;0
WireConnection;60;1;62;0
WireConnection;60;2;43;2
WireConnection;0;0;54;0
WireConnection;0;1;55;0
WireConnection;0;4;25;0
WireConnection;0;9;60;0
ASEEND*/
//CHKSM=A17D77DECA0940633FACD938781FF388C6083752