// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Petal_S"
{
	Properties
	{
		[HideInInspector] __dirty( "", Int ) = 1
		_MaskClipValue( "Mask Clip Value", Float ) = 0.5
		_Float0("Float 0", Float) = 2
		_hard_leavy("hard_leavy", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "Transparent+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _hard_leavy;
		uniform float4 _hard_leavy_ST;
		uniform float _Float0;
		uniform float _MaskClipValue = 0.5;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_hard_leavy = i.uv_texcoord * _hard_leavy_ST.xy + _hard_leavy_ST.zw;
			float4 tex2DNode5 = tex2D( _hard_leavy,uv_hard_leavy);
			o.Emission = ( tex2DNode5 * _Float0 ).xyz;
			o.Alpha = 1;
			clip( tex2DNode5.a - _MaskClipValue );
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=7003
831;175;867;564;1156.499;321.0998;1.6;True;False
Node;AmplifyShaderEditor.RangedFloatNode;3;-387.5,196;Float;False;Property;_Float0;Float 0;1;0;2;0;0;FLOAT
Node;AmplifyShaderEditor.SamplerNode;5;-643.0001,-19.90003;Float;True;Property;_hard_leavy;hard_leavy;1;0;Assets/Models/hard_leavy.png;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;2;-164.5,42;Float;False;0;FLOAT4;0.0;False;1;FLOAT;0,0,0,0;False;FLOAT4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;145,-65;Float;False;True;2;Float;ASEMaterialInspector;0;Standard;Petal_S;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;Custom;0.5;True;True;0;True;TransparentCutout;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;False;0;4;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;Add;Add;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;Relative;0;;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;OBJECT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;13;OBJECT;0.0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False
WireConnection;2;0;5;0
WireConnection;2;1;3;0
WireConnection;0;2;2;0
WireConnection;0;10;5;4
ASEEND*/
//CHKSM=00774E92E35546483EC2E357BFFEF0C1B131A1E8