// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Vortex"
{
	Properties
	{
		[HideInInspector] __dirty( "", Int ) = 1
		_T_Perlin_Noise_M("T_Perlin_Noise_M", 2D) = "white" {}
		_TextureSample1("Texture Sample 1", 2D) = "white" {}
		_TextureSample3("Texture Sample 3", 2D) = "white" {}
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityShaderVariables.cginc"
		#include "Tessellation.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 4.6
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) fixed3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float3 worldPos;
			float3 worldNormal;
			INTERNAL_DATA
		};

		struct appdata
		{
			float4 vertex : POSITION;
			float4 tangent : TANGENT;
			float3 normal : NORMAL;
			float4 texcoord : TEXCOORD0;
			float4 texcoord1 : TEXCOORD1;
			float4 texcoord2 : TEXCOORD2;
			float4 texcoord3 : TEXCOORD3;
			fixed4 color : COLOR;
			UNITY_VERTEX_INPUT_INSTANCE_ID
		};

		uniform sampler2D _TextureSample1;
		uniform sampler2D _TextureSample3;
		uniform sampler2D _T_Perlin_Noise_M;

		float4 tessFunction( appdata v0, appdata v1, appdata v2 )
		{
			float4 temp_cast_10 = (2.0).xxxx;
			return temp_cast_10;
		}

		void vertexDataFunc( inout appdata v )
		{
			float3 ase_worldPos = mul(unity_ObjectToWorld, v.vertex);
			float3 temp_output_25_0 = ( ase_worldPos / 10.0 );
			float4 temp_cast_2 = (tex2Dlod( _T_Perlin_Noise_M,float4( (abs( temp_output_25_0.xy+_Time[1] * float2(1,1 ))), 0.0 , 0.0 )).r).xxxx;
			float4 tex2DNode22 = tex2Dlod( _TextureSample1,float4( (abs( temp_output_25_0.xy+_Time[1] * float2(-1,1 ))), 0.0 , 0.0 ));
			float4 tex2DNode23 = tex2Dlod( _TextureSample3,float4( (abs( temp_output_25_0.xy+_Time[1] * float2(-1,-1 ))), 0.0 , 0.0 ));
			float4 temp_cast_7 = (tex2DNode23.r).xxxx;
			v.vertex.xyz += ( ( ( ( temp_cast_2 + tex2DNode22 ) + temp_cast_7 ) * float4( v.normal , 0.0 ) ) / 10.0 ).xyz;
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float3 ase_worldPos = i.worldPos;
			float3 temp_output_25_0 = ( ase_worldPos / 10.0 );
			float4 tex2DNode22 = tex2D( _TextureSample1,(abs( temp_output_25_0.xy+_Time[1] * float2(-1,1 ))));
			float4 tex2DNode23 = tex2D( _TextureSample3,(abs( temp_output_25_0.xy+_Time[1] * float2(-1,-1 ))));
			float3 worldViewDir = normalize( UnityWorldSpaceViewDir( i.worldPos ) );
			float3 ase_worldNormal = i.worldNormal;
			float fresnelFinalVal34 = (-0.3 + 20.0*pow( 1.0 - dot( ase_worldNormal, worldViewDir ) , 5.0));
			o.Emission = lerp( ( lerp( lerp( float4(0.4721095,0,0.7205882,0) , float4(0.375,0.3793105,1,0) , ( tex2DNode22.r + 0.5 ) ) , float4(0,0,0,0) , clamp( ( tex2DNode23.r + 0.2 ) , 0.0 , 1.0 ) ) * 1.5 ) , float4(0.5367647,0.9233267,1,0) , clamp( fresnelFinalVal34 , 0.0 , 1.0 ) ).rgb;
			o.Alpha = 1;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Standard keepalpha vertex:vertexDataFunc tessellate:tessFunction nolightmap 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			# include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float3 worldPos : TEXCOORD6;
				float4 tSpace0 : TEXCOORD1;
				float4 tSpace1 : TEXCOORD2;
				float4 tSpace2 : TEXCOORD3;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				fixed3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				fixed tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				fixed3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			fixed4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				fixed3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=7003
758;209;1434;714;1263.537;75.01969;1.6;True;False
Node;AmplifyShaderEditor.WorldPosInputsNode;24;-1435.442,129.2818;Float;False;FLOAT3;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;26;-1406.941,270.3825;Float;False;Constant;_Float0;Float 0;3;0;10;0;0;FLOAT
Node;AmplifyShaderEditor.SimpleDivideOpNode;25;-1205.04,158.082;Float;False;0;FLOAT3;0.0;False;1;FLOAT;0.0,0,0;False;FLOAT3
Node;AmplifyShaderEditor.PannerNode;5;-1016.104,139.9996;Float;False;-1;1;0;FLOAT2;0,0;False;1;FLOAT;0.0;False;FLOAT2
Node;AmplifyShaderEditor.PannerNode;12;-1026.938,340.2;Float;False;-1;-1;0;FLOAT2;0,0;False;1;FLOAT;0.0;False;FLOAT2
Node;AmplifyShaderEditor.SamplerNode;23;-832.8396,316.3826;Float;True;Property;_TextureSample3;Texture Sample 3;0;0;Assets/Models/Materials/T_Perlin_Noise_M.BMP;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;22;-833.8407,129.1824;Float;True;Property;_TextureSample1;Texture Sample 1;0;0;Assets/Models/Materials/T_Perlin_Noise_M.BMP;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.PannerNode;2;-1012.705,-51.70012;Float;False;1;1;0;FLOAT2;0,0;False;1;FLOAT;0.0;False;FLOAT2
Node;AmplifyShaderEditor.SimpleAddOpNode;36;-78.53735,231.8818;Float;False;0;FLOAT;0.0;False;1;FLOAT;0.2;False;FLOAT
Node;AmplifyShaderEditor.ColorNode;10;-635.938,-251.1717;Float;False;Constant;_Color1;Color 1;2;0;0.375,0.3793105,1,0;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;35;-321.7379,-105.7183;Float;False;0;FLOAT;0.0;False;1;FLOAT;0.5;False;FLOAT
Node;AmplifyShaderEditor.SamplerNode;21;-833.6428,-62.81806;Float;True;Property;_T_Perlin_Noise_M;T_Perlin_Noise_M;0;0;Assets/Models/Materials/T_Perlin_Noise_M.BMP;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ColorNode;8;-653.2002,-420.5005;Float;False;Constant;_Color0;Color 0;2;0;0.4721095,0,0.7205882,0;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.LerpOp;15;-195.3998,-271.9015;Float;False;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0,0,0,0;False;COLOR
Node;AmplifyShaderEditor.ColorNode;45;-126.9393,-25.71931;Float;False;Constant;_Color3;Color 3;3;0;0,0,0,0;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;37;57.66188,142.5815;Float;False;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;42;-311.8402,620.1808;Float;False;Constant;_Float4;Float 4;3;0;-0.3;0;0;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;29;-434.4408,166.3829;Float;False;0;FLOAT;0.0;False;1;FLOAT4;0.0;False;FLOAT4
Node;AmplifyShaderEditor.RangedFloatNode;43;-303.1402,700.6808;Float;False;Constant;_Float5;Float 5;3;0;20;0;0;FLOAT
Node;AmplifyShaderEditor.FresnelNode;34;-112.2405,659.8828;Float;False;0;FLOAT3;0,0,0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;3;FLOAT;5.0;False;FLOAT
Node;AmplifyShaderEditor.LerpOp;19;211,3.99802;Float;False;0;COLOR;0.0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0,0,0,0;False;COLOR
Node;AmplifyShaderEditor.RangedFloatNode;39;282.7604,167.6813;Float;False;Constant;_Float2;Float 2;3;0;1.5;0;0;FLOAT
Node;AmplifyShaderEditor.NormalVertexDataNode;4;-425.8001,462.8;Float;False;FLOAT3;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;30;-244.0404,338.1827;Float;False;0;FLOAT4;0.0;False;1;FLOAT;0.0,0,0,0;False;FLOAT4
Node;AmplifyShaderEditor.ClampOpNode;44;454.6598,647.3815;Float;False;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;38;384.7604,4.681305;Float;False;0;COLOR;0.0;False;1;FLOAT;0,0,0,0;False;COLOR
Node;AmplifyShaderEditor.ColorNode;17;282.5006,231.6995;Float;False;Constant;_Color2;Color 2;3;0;0.5367647,0.9233267,1,0;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;33;77.55937,527.0831;Float;False;Constant;_Float1;Float 1;3;0;10;0;0;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;31;94.05923,429.5829;Float;False;0;FLOAT4;0.0;False;1;FLOAT3;0.0,0,0,0;False;FLOAT4
Node;AmplifyShaderEditor.LerpOp;40;590.259,92.7808;Float;False;0;COLOR;0.0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0.0;False;COLOR
Node;AmplifyShaderEditor.RangedFloatNode;46;637.5604,369.6803;Float;False;Constant;_Float6;Float 6;3;0;2;0;0;FLOAT
Node;AmplifyShaderEditor.TextureCoordinatesNode;3;-1434.104,-61.30011;Float;False;0;-1;2;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;FLOAT2;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleDivideOpNode;32;267.7598,478.7831;Float;False;0;FLOAT4;0.0;False;1;FLOAT;0.0,0,0,0;False;FLOAT4
Node;AmplifyShaderEditor.RangedFloatNode;41;-306.8402,780.1808;Float;False;Constant;_Float3;Float 3;3;0;5;0;0;FLOAT
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;796.2001,46.70001;Float;False;True;6;Float;ASEMaterialInspector;0;Standard;Vortex;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;Opaque;0.5;True;True;0;False;Opaque;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;True;0;4;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;Add;Add;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;Relative;0;;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;OBJECT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;13;OBJECT;0.0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False
WireConnection;25;0;24;0
WireConnection;25;1;26;0
WireConnection;5;0;25;0
WireConnection;12;0;25;0
WireConnection;23;1;12;0
WireConnection;22;1;5;0
WireConnection;2;0;25;0
WireConnection;36;0;23;1
WireConnection;35;0;22;1
WireConnection;21;1;2;0
WireConnection;15;0;8;0
WireConnection;15;1;10;0
WireConnection;15;2;35;0
WireConnection;37;0;36;0
WireConnection;29;0;21;1
WireConnection;29;1;22;0
WireConnection;34;1;42;0
WireConnection;34;2;43;0
WireConnection;19;0;15;0
WireConnection;19;1;45;0
WireConnection;19;2;37;0
WireConnection;30;0;29;0
WireConnection;30;1;23;1
WireConnection;44;0;34;0
WireConnection;38;0;19;0
WireConnection;38;1;39;0
WireConnection;31;0;30;0
WireConnection;31;1;4;0
WireConnection;40;0;38;0
WireConnection;40;1;17;0
WireConnection;40;2;44;0
WireConnection;32;0;31;0
WireConnection;32;1;33;0
WireConnection;0;2;40;0
WireConnection;0;11;32;0
WireConnection;0;14;46;0
ASEEND*/
//CHKSM=6AD54F36359D19AF1BD1DFBDB3E17B4A30FB3E40